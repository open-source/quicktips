FROM node:20

COPY . /usr/src/app
WORKDIR /usr/src/app

CMD bash -c "node index.js"