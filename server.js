'use strict';

const restify = require('restify');
const FS = require('fs');

const Config = require('./config.js');

const server = restify.createServer();
var versionedServers = registerAllServerVersions(server);

function registerAllServerVersions(server) {
  var versionFolders = FS.readdirSync('.').filter(function (s) {
    return s.match(/^\d+\.\d+\.\d+$/)
  }); // all loader files
  return versionFolders.map(function (versionFolder) {
    const versionedServer = require('./' + versionFolder + '/server.js');
    // Returns the server object for the requested version
    // If the server was requested before, the same object is returned
    versionedServer.register(server);
    return versionedServer;
  });
}

//expose statistical information
server.get('/stats.json', function (req, res, next) {
   res.send({ versionedServers: versionedServers.map((vs) => vs.stats()) });
   next();
});

server.listen(80, function () {
  console.log('%s listening at %s', server.name, server.url);
});
