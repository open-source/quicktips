'use strict';

const builder = require('xmlbuilder');

exports.load = function (tips, loaded_handler) {
    var doc = builder
        .create('mg:tips')
        .att('xmlns', 'http://www.w3.org/1999/xhtml')
        .att('xmlns:mg', 'http://metager.de/tips')
        .att('xml:lang', 'de');

    tips.forEach(function (tip) {
        const entry = doc.ele('mg:tip').att('type', 'text').text(tip);
        if(tip.url != null && tip.url != ""){
            entry.ele('link').att('href', tip.url);
        }
    });

    doc.end({
        pretty: true
    });

    loaded_handler(doc.toString());
};
