'use strict';

const xml2js = require('xmlbuilder');
const URL = require('url');

const METAGER_URL = 'https://quicktips.metager3.de'; // TODO find out dynamicly or make parser work without it

exports.load = function (results, request, loadedHandler) {
  const reqUrl = new URL.URL(METAGER_URL + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  const search = reqUrl.searchParams.get('search'); // requested search term

  const builder = require('xmlbuilder');
  var doc = builder
    .create('feed')
    .att('xmlns', 'http://www.w3.org/2005/Atom')
    .att('xmlns:opensearch', 'http://a9.com/-/spec/opensearch/1.1/')
    .att('xmlns:relevance', 'http://a9.com/-/opensearch/extensions/relevance/1.0/')
    .att('xmlns:mg', 'http://metager.de/opensearch/quicktips/');
  doc.ele('title').text('MetaGer quicktip search: ' + search);
  doc.ele('link', {
    rel: 'self',
    href: reqUrl.toString()
  });
  doc.ele('updated', new Date().toISOString());
  doc.ele('opensearch:totalResults', results.length);
  doc.ele('id', 'urn:uuid:cca75ee8-5dc9-4761-b7a2-88b5ab977f51');
  doc.ele('author').ele('name', 'MetaGer');

  results.forEach(function (result) {
    if (!result.title) result.title = '';
    if (!result.url) result.url = '';
    const entry = doc.ele('entry');
    entry.ele('title', result.title);
    entry.ele('link').att('href', result.url);
    entry.ele('mg:type', result.type);
    entry.ele('mg:gefVon', result.gefVon);
    entry.ele('mg:gefVonTitle', result.gefVonTitle);
    entry.ele('mg:gefVonLink', result.gefVonLink);
    entry.ele('relevance:score').text(result.priority);
    entry.ele('id', 'urn:uuid:00000000-0000-0000-0000-000000000000');
    entry.ele('updated', new Date().toISOString());
    entry.ele('content').att('type', 'text').text(result.summary);
    if (result.author) {
      entry.ele('mg:author', result.author);
    }
    if (result.details) {
      const detailsEle = entry.ele('mg:details');
      result.details.forEach(function (detail) {
        const detailEle = detailsEle.ele('entry');
        detailEle.ele('title', detail.title);
        detailEle.ele('text', detail.text);
        detailEle.ele('url', detail.url);
      });
    }
  });

  doc.end({
    pretty: true
  });

  loadedHandler(doc.toString());
};