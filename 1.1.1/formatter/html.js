'use strict';

const URL = require('url');
const Blade = require('blade');
const { t } = require('localizify');

const METAGER_URL = 'https://quicktips.metager3.de'; // TODO find out dynamicly or make parser work without it

exports.load = function (results, request, loadedHandler) {
  const reqUrl = new URL.URL(METAGER_URL + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  const search = reqUrl.searchParams.get('search'); // requested search term

  var x = Blade.compileFile('storage/html-template.html', (err, tmpl) => {
    if (err) {
      console.log(err.message);
      loadedHandler(null);
    } else {
      tmpl({quicktips: results}, (err, html) => {
        loadedHandler(html);
      });
    }
  });
};
