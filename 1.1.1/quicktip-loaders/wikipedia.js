'use strict';

const Request = require('request');
const URL = require('url');
const {
  t
} = require('localizify');

const Config = require('../../config.js');
const PRIORITY = 0.2;

exports.toString = function () {
  return 'wikipedia.js';
};

exports.load = function (search, req, resultHandler) {
  if (!search || search.length == 0) {
    resultHandler(null);
  } else {
    const reqUrl = new URL.URL('http://www.dummy.org' + req.url); // URL object of the requested URL, the added prefix is needed for the parser to work
    var locale = reqUrl.searchParams.get('locale') || Config.DEFAULT_LOCALE; // read the locale url parameter
    if (locale == 'all') {
      locale = 'de';
    }
    const requestUrl = 'https://' + locale + '.wikipedia.org/w/api.php?action=opensearch&search=' + encodeURIComponent(search) + '&limit=10&namespace=0&format=json&redirects=resolve'; // construct the url for wikipedia with the given locale and search

    Request(requestUrl, function (error, response, body) {
      let responseBody = "";
      try{
        responseBody = JSON.parse(body);
      }catch (e) {
        resultHandler(null);
        return;
      }
      // Check whether there are any articles
      if (responseBody.length === 0 || typeof responseBody[1] === "undefined" || responseBody[1].length === 0) {
        resultHandler(null);
        return;
      }
      // create the initial result
      const result = {
        type: 'wikipedia',
        title: responseBody[1][0],
        url: responseBody[3][0],
        summary: responseBody[2][0],
        details: [],
        gefVon: t('from') + ' <a href="https://wikipedia.org/" target="_blank" rel="noopener">Wikipedia</a>',
        gefVonTitle: 'Wikipedia',
        gefVonLink: 'https://wikipedia.org/',
        priority: PRIORITY
      };
      // add all further results as details
      for (var i in responseBody[1]) {
        if (i != 0) {
          result.details.push({
            title: responseBody[1][i],
            text: responseBody[2][i],
            url: responseBody[3][i]
          });
        }
      }
      if (result.details.length == 0) {
        delete result.details;
      }
      // return the result
      resultHandler(result);
    });
  }
};