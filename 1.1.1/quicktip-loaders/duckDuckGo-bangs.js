'use strict';

const Request = require('request');
const URL = require('url');
const {
  t
} = require('localizify');

const PRIORITY = 0.1;

function fixedEncodeURIComponent (str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
    return '%' + c.charCodeAt(0).toString(16);
  });
}

exports.toString = function () {
  return 'duckDuckGo-bangs.js';
};

exports.load = function (search, request, resultHandler) {
  if (!search || search.length == 0) {
    resultHandler(null);
  } else {
    const placeholder = '0X0plchldr0X0';
    const searchWords = search.split(' ');
    var foundbang = false;
    var dummyQuery = '';
    var realQuery = '';

    searchWords.forEach(function (searchWord) {
      if (searchWord[0] === '!') {
        dummyQuery += searchWord + ' ';
        foundbang = true;
      } else {
        realQuery += searchWord + ' ';
      }
    });

    realQuery = realQuery.trim();

    if (dummyQuery.length > 0 && foundbang) {
      dummyQuery += placeholder;
      const requestUrl = 'https://api.duckduckgo.com/?format=json&no_redirect=1&t=MetaGerDE&q=' + fixedEncodeURIComponent(dummyQuery);
      Request(requestUrl, function (error, response, body) {
        const responseBody = JSON.parse(body);
        try {
          const bangUrl = responseBody['Redirect'].replace(placeholder, fixedEncodeURIComponent(realQuery));
          const UrlObj = new URL.URL(bangUrl);
          const result = {
            type: 'duckDuckGo-bang',
            title: t('bang redirection'),
            url: bangUrl,
            summary: '<a href="' + bangUrl + '" target=_blank class="btn bang-btn">' + t('continue to') + ' ' + UrlObj.host + '</a>',
            gefVon: t('from') + ' <a href="https://api.duckduckgo.com/" target="_blank" rel="noopener">DuckDuckGo</a>',
            gefVonTitle: 'DuckDuckGo',
            gefVonLink: 'https://api.duckduckgo.com/',
            highlight: true,
            priority: PRIORITY
          };
          resultHandler(result);  // return the result
        } catch (_) {
          resultHandler(null);
        }
      });
    } else {
      resultHandler(null);
    }
  }
};
