'use strict';

const Request = require('request');
const URL = require('url');
const {
  t
} = require('localizify');

const Config = require('../../config.js');
const PRIORITY = 0.3;

exports.toString = function () {
  return 'dictCC.js';
};

exports.load = function (search, req, resultHandler) {
  if (!search || search.length == 0 || search.split(' ').filter(item => !["to", "a"].includes(item)).length > 2) {
    resultHandler(null);
  } else {
    const reqUrl = new URL.URL('http://www.dummy.org' + req.url); // URL object of the requested URL, the added prefix is needed for the parser to work
    const locale = reqUrl.searchParams.get('locale') || Config.DEFAULT_LOCALE; // read the locale url parameter
    // only load results if the locale is de
    if (locale == 'de' || locale == 'all') {
      const requestUrl = 'http://www.dict.cc/metager.php?s=' + encodeURIComponent(search); // construct the url for dictCC with the given locale and search term
      Request(requestUrl, function (error, response, body) {
        const responseBody = JSON.parse(body);
        // Check whether there are any translations
        if (responseBody.translations.length === 0) {
          resultHandler(null);
          return;
        }
        // create the initial result
        const result = {
          type: 'dictCC',
          title: responseBody.headline,
          url: responseBody.link,
          summary: responseBody.translations[0],
          details: [],
          gefVon: t('from') + ' <a href="https://dict.cc/" target="_blank" rel="noopener">dict.cc</a>',
          gefVonTitle: 'dict.cc',
          gefVonLink: 'https://dict.cc/',
          priority: PRIORITY
        };
        // add all further results as details
        for (var i = 1; i < responseBody.translations.length; i++) {
          result.details.push({
            title: responseBody.translations[i]
          });
        }
        if (result.details.length == 0) {
          delete result.details;
        }
        // return the result
        resultHandler(result);
      });
    } else {
      resultHandler(null);
    }
  }
};
