'use strict';

const FS = require('fs');

const PRIORITY = 0;

exports.toString = function () {
  return "ads.js";
}

exports.load = function (search, request, resultHandler) {
  const adFile = FS.readFileSync(__dirname + '/../storage/ads.txt');
  const ads = JSON.parse(adFile.toString());
  if (ads.length > 0) {
    const ad = ads[Math.round(Math.random() * (ads.length - 1))];
    const result = {
      type: 'ad',
      title: ad.title,
      summary: ad.summary,
      url: ad.URL,
      priority: PRIORITY
    };
    resultHandler(result);
  } else {
    resultHandler(null);
  }
};