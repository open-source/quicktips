'use strict';

const FS = require('fs');
const URL = require('url');
const PRIORITY = 0.9;

const SPRUECHE_FILE_NAME = 'storage/sprueche.csv';

exports.toString = function () {
  return "sprueche.js";
}

exports.load = function (search, request, resultHandler) {
  const reqUrl = new URL.URL('http://www.dummy.org' + request.url);
  // Disable Quotes if query Parameter quotes is set to off
  if(reqUrl.searchParams.get('quotes') == "off"){
    resultHandler(null);
    return
  }
  const spruecheFile = FS.readFileSync(__dirname + '/../' + SPRUECHE_FILE_NAME);
  const sprueche = spruecheFile.toString().split('\n');
  const spruch = sprueche[Math.floor(Math.random() * (sprueche.length - 1))];
  const spruch_parts = spruch.split('\t');
  const spruch_text = spruch_parts[0];
  const spruch_author = spruch_parts[1];
  const result = {
    type: 'spruch',
    author: spruch_author,
    summary: spruch_text,
    priority: PRIORITY
  };
  resultHandler(result);
};