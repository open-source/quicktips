'use strict';

const Config = require('../../config.js');
const FS = require('fs');
const URL = require('url');
const {
  t
} = require('localizify');

const PRIORITY = 0.4;

exports.toString = function () {
  return 'tips.js';
};

exports.load = function (search, request, resultHandler) {
  const reqUrl = new URL.URL('http://www.dummy.org' + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  var locale = reqUrl.searchParams.get('locale') || Config.DEFAULT_LOCALE; // read the locale url parameter
  if (locale == 'all') {
    locale = 'de';
  }
  let tipFile = FS.readFileSync(__dirname + '/../storage/de-tips.txt');
  if (locale == "en") {
    tipFile = FS.readFileSync(__dirname + '/../storage/en-tips.txt');
  }
  const tips = tipFile.toString().trim().split('\n');
  const tip = tips[Math.round(Math.random() * (tips.length - 1))];
  const result = {
    type: 'tip',
    title: t('did you know'),
    url: '/tips',
    summary: tip,
    priority: PRIORITY
  };
  resultHandler(result);
};
