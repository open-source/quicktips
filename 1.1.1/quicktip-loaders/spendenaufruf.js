'use strict';

const { t } = require('localizify');

const PRIORITY = 1;

exports.toString = function () {
  return 'tips.js';
};

exports.load = function (search, request, resultHandler) {
  const result = {
    type: 'spendenaufruf',
    title: t('dummytext'),
    summary: '<span><a href=\"/spende\" class=\"btn spendenaufruf-btn\" target=\"_blank\">' + t('donate') + '</a></span>',
    priority: PRIORITY
  };
  resultHandler(result);
};
