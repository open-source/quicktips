'use strict';

const Tips = require('../quicktip-loaders/tips.js');

const FS = require('fs');

exports.load = function (formatter, loaded_handler) {
    const tipFile = FS.readFileSync(__dirname + '/../storage/de-tips.txt');
    const tips = tipFile.toString().trim().split('\n');
    formatter(tips, loaded_handler);
};

exports.loadEn = function (formatter, loaded_handler) {
    const tipFile = FS.readFileSync(__dirname + '/../storage/en-tips.txt');
    const tips = tipFile.toString().trim().split('\n');
    formatter(tips, loaded_handler);
};
