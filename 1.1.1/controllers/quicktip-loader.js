'use strict';

const FS = require('fs');
const URL = require('url');

const Config = require('../../config.js');

var countLoaders = null;

exports.load = function (request, formatter, loaded_handler) {
  const results = []; // array of the results of all loaders
  var loaderFiles = FS.readdirSync(__dirname + '/../quicktip-loaders'); // all loader files
  const reqUrl = new URL.URL('http://www.dummy.org' + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  const search = reqUrl.searchParams.get('search'); // requested search term

  var blacklist = ["dictCC"]; // blacklist for unwanted loaders
  reqUrl.searchParams.forEach(function (value, key) {
    if (key.startsWith('loader') && value === 'false') {
      blacklist.push(key.substr('loader_'.length));
    }
  }); // fill blacklist by reading the url parameters

  loaderFiles = loaderFiles.filter(function (loader) {
    return blacklist.reduce(function (prev, current) {
      return prev && current != loader.substr(0, loader.length - '.js'.length);
    }, true);
  }); // remove all loaders that are in the blacklist

  var loading = loaderFiles.length; // amount of loaders working
  countLoaders = loading; // save how many loaders exist

  // Safety timeout for loaders taking too long
  // Wait for 1 second and return available results if not yet done
  const timeoutHandle = setTimeout(function () {
    console.log('Reached Loader Timeout of ' + Config.TIMEOUT + 'ms.');
    loading = 0;
    // format the results
    formatter(results, request, loaded_handler);
  }, Config.TIMEOUT);

  for (var loaderFile in loaderFiles) {
    const loader = require('../quicktip-loaders/' + loaderFiles[loaderFile]);
    /* try to call load for every loader available in 'quicktip-loaders' folder
     * every loader implements the load function, 
     * taking the search word and request as a parameter,
     * as well as a function to call when the result is fetched
     */
    try {
      loader.load(search, request, function (result) {
        if (result !== null) {
          results.push(result);
        }
        loading--;
        // if all other loaders are already done
        if (loading === 0) {
          // format the results
          formatter(results, request, loaded_handler);
          clearTimeout(timeoutHandle);
        }
      });
    } catch (exception) {
      console.error('Loader ' + loader + ' exited with exception ' + exception);
      loading--;
    }
  }
};

exports.stats = function () {
    return {
        countLoaders: countLoaders
    };
}
