'use strict';

const localizify = require('localizify');

const en = require('../translations/en.json');
const de = require('../translations/de.json');

localizify
  .add('en', en)
  .add('de', de)
  .add('all', de)
  .setLocale('en');
