'use strict';

const VERSION = '1.1';

const Localizer = require('./controllers/localizer.js');

const restify = require('restify');
const URL = require('url');
const localizify = require('localizify');

const Config = require('../config.js');

const quicktip_loader = require('./controllers/quicktip-loader.js');
const tip_loader = require('./controllers/tip-loader.js');

const formatter_xml_opensearch = require('./formatter/xml-opensearch.js');
const formatter_html = require('./formatter/html.js');
const formatter_xml_tips = require('./formatter/xml-tips.js');

function respond_xml(request, res, next) {
  const reqUrl = new URL.URL('http://www.dummy.org' + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  const locale = reqUrl.searchParams.get('locale') || Config.DEFAULT_LOCALE; // read the locale url parameter
  localizify.setLocale(locale);
  quicktip_loader.load(request, formatter_xml_opensearch.load, function (results) {
    res.header('content-type', 'application/xml');
    res.end(results);
    next();
  });
}

function respond_html(request, res, next) {
  const reqUrl = new URL.URL('http://www.dummy.org' + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  const locale = reqUrl.searchParams.get('locale') || Config.DEFAULT_LOCALE; // read the locale url parameter
  localizify.setLocale(locale);
  quicktip_loader.load(request, formatter_html.load, function (results) {
    res.header('content-type', 'text/html');
    res.end(results);
    next();
  });
}

function respond_tips(request, res, next) {
  const reqUrl = new URL.URL('http://www.dummy.org' + request.url); // URL object of the requested URL, the added prefix is needed for the parser to work
  const locale = reqUrl.searchParams.get('locale') || Config.DEFAULT_LOCALE; // read the locale url parameter
  if (locale == "de") {
    tip_loader.load(formatter_xml_tips.load, function (results) {
      res.header('content-type', 'application/xhtml+xml');
      res.end(results);
      next();
    });
  } else {
    tip_loader.loadEn(formatter_xml_tips.load, function (results) {
      res.header('content-type', 'application/xhtml+xml');
      res.end(results);
      next();
    });
  }
}

exports.register = function (server) {
  server.get('/' + VERSION + '/quicktips.xml', respond_xml);
  server.head('/' + VERSION + '/quicktips.xml', respond_xml);
  server.get('/' + VERSION + '/quicktips.html', respond_html);
  server.head('/' + VERSION + '/quicktips.html', respond_html);
  server.get('/' + VERSION + '/tips.xml', respond_tips);
  server.head('/' + VERSION + '/tips.xml', respond_tips);
}

exports.stats = function () {
  return {
    version: VERSION,
    quicktip_loader: quicktip_loader.stats()
  }
}
